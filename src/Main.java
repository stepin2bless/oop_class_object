public class Main {
    public static void main(String[]args) {


        Wrangler myWranglerCar = new Wrangler();
        myWranglerCar.drive(100);
        System.out.println(myWranglerCar.odometer);
        myWranglerCar.takeOffDoors();
        System.out.println(myWranglerCar.returnCarModel());


        ModelX myModelXCar = new ModelX();
        myModelXCar.drive (90);
        System.out.println(myModelXCar.odometer);
        myModelXCar.switchAutopilotOn();
        System.out.println(myModelXCar.returnCarModel());

        Car MyCar = new Car();
        MyCar.drive(50);
        MyCar.returnCarModel();

    }
}
