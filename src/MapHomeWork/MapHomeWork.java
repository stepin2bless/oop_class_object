package MapHomeWork;

import java.util.HashMap;

public class MapHomeWork {
    public static void main (String[] args) {

        HashMap <Integer, String> address = new HashMap<>();
        address.put(1000, "Liam");
        address.put(1001, "Noah");
        address.put(1002, "Olivia");
        address.put(1003, "Emma");
        address.put(1004, "Benjamin");
        address.put(1005, "Evelyn");
        address.put(1006, "Lucas");

        System.out.println(address.get(1004));  //Benjamin

        HashMap <Integer, String> oddAddress = new HashMap<>();
        oddAddress.put(1001, "Noah");
        oddAddress.put(1003, "Emma");
        oddAddress.put(1005, "Evelyn");
        System.out.println (oddAddress);


    }

}
