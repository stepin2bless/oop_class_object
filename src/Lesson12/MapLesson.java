package Lesson12;

import java.util.HashMap;

public class MapLesson {

    public static void main (String[] args){
//        HashMap<String, String> emailMap = new HashMap<String, String>();
//        emailMap.put("azat7", "azat@testpro.io");
//        emailMap.put("khaled8", "khaled@testpro.io");


//        System.out.println(emailMap.get("khaled8"));

     //   emailMap.remove("khaled8");
     //   emailMap.clear();

       // System.out.println(emailMap);

//        HashMap<Integer, String> emailMap = new  HashMap<Integer, String>();
//        emailMap.put(1,"azat@testpro.io" );
//        emailMap.put(2,"khaled@testpro.io" );

       // System.out.println(emailMap.size()); //counting the key

//            for (Integer key: emailMap.keySet()) {
//               System.out.println(key);
//           }
//            for (String value: emailMap.values()){
//                System.out.println(value);
//            }
//
//            for (Integer key : emailMap.keySet()){
//                System.out.println("key: " + key + " -- value: " + emailMap.get(key));
//            }


       // new
//        HashMap<Integer, Boolean> emailMap = new  HashMap<Integer, Boolean>();
//        emailMap.put(1, true );
//        emailMap.put(2,false );
//
//        System.out.println(emailMap.get(2)); //false
//        System.out.println(emailMap.get(1)); //true

// new
//        HashMap<Integer, String> emailMap = new  HashMap<Integer, String>();
//        emailMap.put(1,"azat@testpro.io" );
//        emailMap.put(2,"khaled@testpro.io" );

       // System.out.println(emailMap.containsKey(2)); //true
        // System.out.println(emailMap.containsValue("khaled@testpro.io" )); //true


//new
//        HashMap<Integer, String> srcMap = new  HashMap<Integer, String>();
//        srcMap.put(1,"azat@testpro.io" );
//        srcMap.put(2,"khaled@testpro.io");
//        HashMap<Integer, String> toMap = new  HashMap<Integer, String>();
//        toMap.put(3,"zaman@testpro.io");
//        toMap.putAll(srcMap);
//
//        System.out.println(srcMap);  // {1=azat@testpro.io, 2=khaled@testpro.io}
//        System.out.println(toMap); // {1=azat@testpro.io, 2=khaled@testpro.io, 3=zaman@testpro.io}


 // new.remove
//           HashMap<Integer, String> srcMap = new  HashMap<Integer, String>();
//           srcMap.put(1,"azat@testpro.io" );
//           srcMap.put(2,"khaled@testpro.io");
//
//           srcMap.remove(1);
//            System.out.println(srcMap); //key 1 removed

// new  Exercise

//        HashMap<String,Integer> srcMap = new HashMap<>();
//        srcMap.put("Jane1", 25);
//        srcMap.put("Jane2",25);
//        System.out.println(srcMap.size()); // can not have the same key . work with different key (whether same or different value)

      //new

        String a= "";
        for (int i=1; i<100; i++) {
           // a = a +"a"; // 1 + "a", a + "a", aa +"a", etc
            a += "a";
            System.out.println(a);
        }


    }
}
