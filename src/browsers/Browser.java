package browsers;

class Browser {
  private String browserName = "";
  private String defaultPage = "default";


    public String getBrowserName() {

        return browserName;
    }

    public void setBrowserName(String browName){
        //what happens if we get "chrome"
         // browserName = browName.trim();
         if (browName.equals("chrome")){
             System.out.println("we are using chrome bowser");
         } else if (browName.equals("ie")){
             System.out.println("why are we using ie");
         } else {
             System.out.println("Using some other browser");
         }
    }

    public void setDefaultPage(String page) {
        defaultPage = page;
    }

    public void openDefaultHomePage() {
        System.out.println("opened " + defaultPage);
    }
}