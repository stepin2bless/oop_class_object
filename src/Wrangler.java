public class Wrangler extends Car {
    // atribute (what it has); mobil wrangler punya odometer dan pintunya bisa dicopot


    boolean carDoors = true;

    // what to do (bisa dijalanin (diukur pakai miles)
    public void drive(int miles) {
        System.out.println("Car drove" + miles + "miles");
        odometer = odometer + miles;
    }

    //pintunya bisa diambil
    public void takeOffDoors() {
        carDoors = false;
        System.out.println("Doors are taken off");
    }

    //pintunya bisa ditaruh balik
    public void putBackDoors() {
        carDoors = true;
        System.out.println("Doors are put back");
    }

    public String returnCarModel() {
        return "Car model is wrangler";
    }
}