package day8_class_and_objects;

public class Car { //all codes should be inside of Class

    int numberDoors = 4; //instance variable belongs to Object
    static String engine = "Enginex3"; //static variable belongs to Class


    String brand = "Mercedes";
    String model; //we can skip assigning value to the instance and static variables where we declare them, in this case default value of String variable is Null
    int numberTear; // default value of int variable is 0, because we do not assign any value

    public Car(){ //Constructors are for creating objects, when we create an Object, constructor will be invoked. If there is no Constructor inside of class, Java will create it by itself implicitly. But if we have created parametrized Constructor (next example), then we have to create default Constructor by ourselves. Java doesn't want to take responsibility to create default (empty) constructor if you have created parametrized Constructor(with parameters).

    }

    public Car(int numDoors, String modelName){// this constructor accepts 2 parameters -> int numDoors, String modelName. When we create an Object we are invoking related Constructor. To invoke this Constructor we have to create an object which accepts 2 parameters -> day5_class_and_objects.Car car1 = new day5_class_and_objects.Car (int numDoors, String modelName)
        numberDoors = numDoors; // to assign new values to variables which is inside of day5_class_and_objects.Car class we have to assign new values to them. New value of numberDoors will be changed by the value coming from Object when created
        model = modelName;
    }

    public Car(int numDoors, String modelName, String brandName){// this constructor accepts 3 parameters -> int numDoors, String modelName, String brandName
        numberDoors = numDoors;
        model = modelName;
        brand = brandName;
    }



    public void park(){ //instance method
        System.out.println("day5_class_and_objects.Car is parking");
    }
    public void drive(){
        System.out.println("day5_class_and_objects.Car is driving");

    }

    public static void stop(){ // static method belongs to class and can be called by class name -> Car.stop();
        System.out.println("day5_class_and_objects.Car is stopping");
    }




}
