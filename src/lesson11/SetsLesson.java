package lesson11;
import javax.management.StringValueExp;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class SetsLesson {
    public static void main(String[] args) {
// LESSON HashSet (for sorting purpose and did not take duplicate value)


//        Set<String> hashSetValue = new HashSet<>();
//        hasSetValue.add("England");
//        hasSetValue.add("USA");
//        hasSetValue.add("Canada");
//        hasSetValue.add("Canada");
//        hasSetValue.add("Mexico");
//
//        System.out.println(hashSetValue);
        //    result  hashSet [Canada, USA, England, Mexico]


        //Lesson in TreeSets
//        result TreeSets [Canada, England, Mexico, USA]
//        Set<String> hasSetValue = new TreeSet<>();
//        hasSetValue.add("England");
//        hasSetValue.add("USA");
//        hasSetValue.add("Canada");
//        hasSetValue.add("Canada");
//        hasSetValue.add("Mexico");
//
//        System.out.println(hasSetValue);


        // SET UnionS
//        Set<Integer>hasSet1 = new HashSet<>(Arrays.asList(1,2,3));
//        hasSet1.add(1);
//        hasSet1.add(2);
//        hasSet1.add(3); (above is how yu can write it differently)

//        Set<Integer>hasSet2 = new HashSet<>(Arrays.asList(10,20,3));
//        hasSet2.add(10);
//        hasSet2.add(20);
//        hasSet2.add(3);
//        Set<Integer>hasSet3 = new HashSet<>(hasSet1);

//        hasSet3.addAll(hasSet2);
//        System.out.println(hasSet3);
//        result Union : [1, 2, 3, 20, 10] using addAll

//        Intersection
//        Set<Integer>hasSet1 = new HashSet<>(Arrays.asList(1,2,3));
//        Set<Integer>hasSet2 = new HashSet<>(Arrays.asList(10,2,3,5,3));
//        Set<Integer>hasSet3 = new HashSet<>(hasSet1);
//        hasSet3.retainAll(hasSet2);
//        System.out.println(hasSet3);
//      result intersection: [2, 3] only taking the same number

        // Difference
//        Set<Integer>hasSet1 = new HashSet<>(Arrays.asList(1,2,3));
//        Set<Integer>hasSet2 = new HashSet<>(Arrays.asList(10,2,3,5,3));
//        Set<Integer>hasSet3 = new HashSet<>(hasSet1); // (1,2,3)
//        hasSet3.removeAll(hasSet2);
//        System.out.println(hasSet3);
//        result difference : [1]


        //from exercice Easy on class
//    Set<Integer> hasSet = new HashSet<Integer>();  //1.a Initial hasSet <Integer>
//    Set<Integer> removeHasSet = new HashSet<Integer>();

// To add value to the hashSet
        // 1.b Using loop add integers from 1 to 10

//        for (int i=1; i<=10; i++) {
//            hasSet.add(i);
//        }
        // Remove all odd numbers from hashSet
//        for (Integer v: hasSet){
//            if (v % 2 != 0){
//                removeHasSet.add(v);
//            }
//        }
        //Try to create set with duplicate values
//        hasSet.removeAll(removeHasSet);
//        System.out.println(hasSet);
        //result [2, 4, 6, 8, 10]

//        Set<Integer> hashSet = new TreeSet<Integer>(Arrays.asList(100,2,3,50));
//        System.out.println(hashSet);
//        result:[2, 3, 50, 100]
//        List<Integer> arrayList = new ArrayList<>(Arrays.asList(100,2,3,50));
//        System.out.println(arrayList);
//        result : [100, 2, 3, 50]

//       System.out.println(get_hs_size()); //main methode
//         System.out.println(contains_blue("blue"));  // result is true
//        add_element("England" );

//        Let's find out the prime number

        //isPrime(10);
        System.out.println(isPrime(37));

    }

    // new *
    public static boolean isPrime(int number) {
        boolean prime = false;
        for (int i = 2; i < number; i++) {
            if (number % i != 0) {
                prime = true;
            }
            else {
                prime = false;
                break;
            }

            }
            return prime;

        }

//    new * //From size exercise

//    public static boolean contains_blue(String args){
//        Set<String> hashSet = new HashSet<String>(Arrays.asList("blue", "green", "yellow"));
//        return hashSet.contains(args);


//        public static void add_element (String value){
//            Set<String> hashSet = new HashSet<String>();
//            hashSet.add(value);
//            System.out.println(hashSet);
//            result : [England]

        //     }
//    public static int get_hs_size() {
//        Set<Integer> hashSet = new HashSet<Integer>(Arrays.asList(1, 2, 3));
//        return hashSet.size(); //result is 3

}




