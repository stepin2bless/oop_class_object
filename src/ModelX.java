public class ModelX extends Car {

    boolean autoPilot = false;

    public void drive(int miles) {
        System.out.println("Car drove " + miles + " miles");
        odometer = odometer + miles;
    }

    public void switchAutopilotOn() {
        autoPilot = true;
        System.out.println("Autopilot is switch on");
    }


    public void switchAutopilotOff() {
        autoPilot = false;
        System.out.println("Autopilot is switch off");
    }

    public String returnCarModel() {
        return "Car model is ModelX";
    }
}